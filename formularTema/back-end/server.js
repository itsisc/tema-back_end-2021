const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");

const app = express();
app.use(bodyParser.json());
const port = 5050;
app.listen(port, () => {
  console.log("Server online on: " + port);
});
app.use("/", express.static("../front-end"));
const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "siscit_back_end",
});
connection.connect(function (err) {
  console.log("Connected to database!");
  const sql =
    "CREATE TABLE IF NOT EXISTS studenti_inscrisi(nume VARCHAR(255), prenume  VARCHAR(255), telefon  VARCHAR(255) , email  VARCHAR(255),specializare VARCHAR(255),facebook VARCHAR(255),cod_legitimatie VARCHAR(255),data_inceput_semestru VARCHAR(10),data_sfarsit_semestru VARCHAR(10))"; //intre paranteze campurile cu datele
  connection.query(sql, function (err, result) {
    if (err) throw err;
  });
});
app.post("/student", (req, res) => {
  let student = {
    nume: req.body.nume,
    prenume: req.body.prenume,
    telefon: req.body.telefon,
    cnp: req.body.cnp,
    email: req.body.email,
    specializare: req.body.specializare,
    facebook: req.body.facebook,
    cod_legitimatie: req.body.cod_legitimatie,
    data_inceput_semestru: req.body.data_inceput,
    data_sfarsit_semestru: req.body.data_sfarsit,
    varsta: req.body.varsta,
    medie: req.body.medie

  };

  var forma_finantare;
  var gen;
  let error = [];
  //aici rezolvati cerintele (づ｡◕‿‿◕｡)づ


  if (error.length === 0) {

    const sql = `INSERT INTO studenti_inscrisi (nume,
      prenume,
      telefon,
      cnp,
      email,
      specializare,
      facebook,
      cod_legitimatie,
      data_inceput_semestru,
      data_sfarsit_semestru,
      varsta, 
      medie,
      forma_finantare,
      gen) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)`;
    connection.query(
      sql,
      [
        student.nume,
        student.prenume,
        student.telefon,
        student.cnp,
        student.email,
        student.specializare,
        student.facebook,
        student.cod_legitimatie,
        student.data_inceput_semestru,
        student.data_sfarsit_semestru,
        student.varsta,
        student.medie


      ],
      function (err, result) {
        if (err) throw err;
        console.log("Student creat cu succes!");
        res.status(200).send({
          message: "Student creat cu succes",
        });
        console.log(sql);
      }
    );
  } else {
    res.status(500).send(error);
    console.log("Studentul nu a putut fi creat!");
  }
  app.use('/', express.static('../front-end'))
});
//modifica si din front la index.html
